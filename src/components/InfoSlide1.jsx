import is1 from "../css/InfoSlide1.module.css";

function InfoSlide1() {
  return (
    <div id={is1.is1mainContainer}>
      <p className={is1.islHeader}>Partner with other Businesses</p>
      <p className={is1.isldata}>
        Across Sectors, Online or Offline, Anywhere in the World. Instantly.
      </p>

      <p className={is1.islHeader}>Market Directly to their Customers</p>
      <p className={is1.isldata}>
        At the Moment of Transaction. Without Customer Data being shared.
      </p>

      <p className={is1.islHeader}>Retain Existing Customers</p>
      <p className={is1.isldata}>At Zero Cost, via Partner Rewards.</p>
    </div>
  );
}

export default InfoSlide1;
