import nb from "../css/Navbar.module.css";
import NavRoutes from "./NavRoutes";

import { Link } from "react-router-dom";

export default function Navbar() {
  return (
    <div id={nb.nbMainContainer}>
      <Link to={"/"}>
        {" "}
        <img
          src={"\\assets\\logo-dark2.png"}
          alt='logo'
        />
      </Link>

      <NavRoutes />
    </div>
  );
}
