
import ptc from "../css/PercentTextContainer.module.css";
import PropTypes from "prop-types";

function PercentTextContainer({ percent, text }) {
  return (
    <div id={ptc.ptcMain}>
      <div id={ptc.ptc1}>{percent}%</div>
      <p id={ptc.ptc2}>{text}</p>
    </div>
  );
}

PercentTextContainer.propTypes = {
  percent: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
};

export default PercentTextContainer;
