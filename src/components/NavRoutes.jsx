import nr from "../css/NavRoutes.module.css";

import { Link } from "react-router-dom";

function NavRoutes() {
  return (
    <div id={nr.nrMain}>
      <Link to={"/"}>Home</Link>
      <Link to={"/"}>
        About Us <i className={nr.dowpdown}></i>{" "}
      </Link>
      <Link to={"/"}>
        Products <i className={nr.dowpdown}></i>{" "}
      </Link>
      <Link to={"/"}>Blog</Link>
      <Link to={"/"}>Team</Link>
      <Link to={"/"}>
        Sign-Up <i className={nr.dowpdown}></i>{" "}
      </Link>
      <Link to={"/"}>
        Login <i className={nr.dowpdown}></i>{" "}
      </Link>
    </div>
  );
}

export default NavRoutes;
