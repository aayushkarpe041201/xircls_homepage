import hs from "../css/HeroSlider.module.css";
import SlideOne from "./SlideOne";

function HeroSlider() {
  return (
    <div id={hs.hsMainContainer}>
      <SlideOne />
    </div>
  );
}

export default HeroSlider;
