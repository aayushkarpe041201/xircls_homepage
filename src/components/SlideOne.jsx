import so from "../css/SlideOne.module.css";

function SlideOne() {
  return (
    <div id={so.soMainContainer}>
      <div id={so.soText}>
        <h2>
          Say Hello To Always-On <br /> <span>Marketing!</span>
        </h2>

        <p
          style={{
            fontSize: "2rem",
            marginBottom: "3rem",
            fontStyle: "italic",
          }}>
          Because Customers shop when they want to.
          <br /> Not when you want them to.
        </p>

        <p style={{ fontSize: "1.2rem", marginBottom: "5rem" }}>
          Stop-Start Marketing Campaigns Lose You Customers Buying Right Now.
        </p>

        <div
          id={so.soTextFinalContainer}
          style={{ marginBottom: "5rem" }}>
          <p>Run a perpetual marketing campaign. </p>
          <hr />
          <p>Reach customers when theyre most likely to buy from you.</p>
          <hr />
          <p>Lock your competition out. </p>
        </div>
      </div>

      <img
        src={"\\assets\\logo12.jpg"}
        alt='img'
      />
    </div>
  );
}

export default SlideOne;
