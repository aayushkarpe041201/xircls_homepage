import is2 from "../css/InfoSlide2.module.css";

function InfoSlide2() {
  return (
    <>
      <div id={is2.is2mainContainer}>
        <p id={is2.is2Heading}>
          {" "}
          Drive high-quality traffic to your website. <br /> Optimize for more
          conversions!
        </p>

        <p style={{ fontSize: "1.5rem", marginBottom: "48px" }}>
          Leverage collaborations to transform your website sales funnel.
        </p>

        <div id={is2.is2Conatainer}>
          <div className={is2.is2Div}>
            <img
              src={"\\assets\\bounce-rate.png"}
              alt='img'
            />

            <p className={is2.is2DivHeading}>Reduce bounce rates.</p>
            <p className={is2.is2Divtxt}>
              Get genuine, verified traffic from your partners to your
              e-commerce storefront.
            </p>
          </div>
          <div className={is2.is2Div}>
            <img
              src={"\\assets\\conversion.png"}
              alt='img'
            />

            <p className={is2.is2DivHeading}>Convert with instant rewards.</p>
            <p className={is2.is2Divtxt}>
              Incentivize actions and purchases with partner offers.
            </p>
          </div>
          <div className={is2.is2Div}>
            <img
              src={"\\assets\\magnet.png"}
              alt='img'
            />

            <p className={is2.is2DivHeading}>Improve customer retention.</p>
            <p className={is2.is2Divtxt}>
              Promise partner offers on future actions & purchases.
              <u>Kick off a perpetual rewards loop!</u>
            </p>
          </div>
        </div>
      </div>
      <div id={is2.is2SignUp}>
        No sign-up fee. No monthly commitment. <u>Sign up here</u>{" "}
        
      </div>
    </>
  );
}

export default InfoSlide2;
