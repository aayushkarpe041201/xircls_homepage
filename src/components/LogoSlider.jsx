import { useState } from "react";
import ls from "../css/LogoSlider.module.css";

function LogoSlider() {
  const [imgUrls, setimgUrls] = useState([
    "\\assets\\img1.png",
    "\\assets\\img2.png",
    "\\assets\\img3.png",
    "\\assets\\img4.png",
    "\\assets\\img5.png",
  ]);
  return (
    <div id={ls.lsMainContainer}>
      <div id={ls.lsLogoSliderContainer}>
        {imgUrls.map((img_src) => (
          <img
            src={img_src}
            alt='img'
            key={img_src}
          />
        ))}

        <img
          src={imgUrls[0]}
          alt='img'
        />
      </div>

      <button id={ls.lsButton}>Join the network </button>
    </div>
  );
}

export default LogoSlider;
