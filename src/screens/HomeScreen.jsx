import HeroSlider from "../components/HeroSlider";
import InfoSlide2 from "../components/InfoSlide2";
import LogoSlider from "../components/LogoSlider";
import InfoSlide1 from "../components/infoSlide1";

function HomeScreen() {
  return (
    <div>
      <HeroSlider />
      <LogoSlider />
      <InfoSlide1 />
      <InfoSlide2 />
      <button
        style={{
          position: "fixed",
          bottom: "20px",
          right: "1rem",
          background: "#2e82cb",
          border: "none",
          width: "10rem",
          height: "3rem",
          fontSize: "1rem",
          fontFamily: "Roboto",
          fontWeight: 800,
          color: "#fff",
          letterSpacing: "0.02em",
        }}>
        CONTACT US
      </button>
    </div>
  );
}

export default HomeScreen;
