import ap from "./css/App.module.css";

import Navbar from "./components/Navbar";
import HomeScreen from "./screens/HomeScreen";

import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  return (
    <div id={ap.apMain}>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route
            path='/'
            element={<HomeScreen />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
